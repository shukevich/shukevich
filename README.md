# Hi 👋 I'm Yaroslav

- 🇱🇹  Currently I live in Vilnius, Lithuania, but I`m from Minsk, Belarus.
- 💻 I'm a senior software engineer at [EPAM Systems](https://epam.com).
- ⌨️ Most of the time I write in Kotlin and Java, sometimes I use Python as well. 
- 🤖 Hands-on experience in leveraging GenAI for backend apps.
- 🕸️ I also build and support cloud-native infrastructure.


#### My resume: [Yaroslav Shukevich.pdf](https://gitlab.com/shukevich/shukevich/-/blob/main/Yaroslav_Shukevich_Resume.pdf)

## 🛠 My fancy badge area

![java](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=openjdk&logoColor=white)![kotlin](https://img.shields.io/badge/Kotlin-0095D5?&style=for-the-badge&logo=kotlin&logoColor=white)![Python](https://img.shields.io/badge/Python-234234?style=for-the-badge&logo=python&labelColor=white&color=red)![spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)![LangChain](https://img.shields.io/badge/Langhain4j-213543245?style=for-the-badge&logo=langchain&logoColor=black&labelColor=white&color=black)![Poetry](https://img.shields.io/badge/Poetry-2342342?style=for-the-badge&logo=poetry&logoColor=red&labelColor=white&color=black)![Apache Kafka](https://img.shields.io/badge/Apache%20Kafka-000?style=for-the-badge&logo=apachekafka&logoColor=white)![ArgoCD](https://img.shields.io/badge/argo-cd?style=for-the-badge&logo=argo&logoColor=orange)![Helm](https://img.shields.io/badge/helm-file?style=for-the-badge&logo=helm&logoColor=orange&color=black)![GitLab CI](https://img.shields.io/badge/gitlab%20ci-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=orange&color=black)![GitHub Actions](https://img.shields.io/badge/github%20actions-%232671E5.svg?style=for-the-badge&logo=githubactions&logoColor=white)![aws](https://img.shields.io/badge/AWS%20-%23FF9900.svg?&style=for-the-badge&logo=amazon-aws&logoColor=white)![azure](https://img.shields.io/badge/Microsoft_Azure-0089D6?style=for-the-badge&logo=microsoft-azure&logoColor=white)![k8s](https://img.shields.io/badge/kubernetes%20-%23326ce5.svg?&style=for-the-badge&logo=kubernetes&logoColor=white)![terraform](https://img.shields.io/badge/terraform%20-%235835CC.svg?&style=for-the-badge&logo=terraform&logoColor=white)![docker](https://img.shields.io/badge/docker-%232496ED.svg?&style=for-the-badge&logo=docker&logoColor=white)![Oracle](https://img.shields.io/badge/Oracle-F80000?style=for-the-badge&logo=oracle&logoColor=white)![postgres](https://img.shields.io/badge/postgres-%23316192.svg?&style=for-the-badge&logo=postgresql&logoColor=white)![maven](https://img.shields.io/badge/maven-java?style=for-the-badge&logo=apachemaven&logoColor=red&color=white!)![gradle](https://img.shields.io/badge/gradle-java?style=for-the-badge&logo=gradle&logoColor=green&color=white)![redis](https://img.shields.io/badge/redis%20-%23CC0000.svg?&style=for-the-badge&logo=redis&logoColor=white)![swagger](https://img.shields.io/badge/swagger-%2385EA2D.svg?&style=for-the-badge&logo=swagger&logoColor=black)![git](https://img.shields.io/badge/git%20-%23F05033.svg?&style=for-the-badge&logo=git&logoColor=white)![IntelliJ IDEA](https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white&color=purple)![prometheus](https://img.shields.io/badge/prometheus-file?style=for-the-badge&logo=prometheus&logoColor=red&color=orange)![Ansys](https://img.shields.io/badge/Ansys-A?style=for-the-badge&logo=ansys&logoColor=white&labelColor=yellow&color=black)![Comsol](https://img.shields.io/badge/comsol-fea?style=for-the-badge&logo=comsol&logoColor=blue&color=white)![Autodesk](https://img.shields.io/badge/autodesk-a?style=for-the-badge&logo=Autodesk&logoColor=red&color=gray)

## ⚙️ My demo projects

### [APIs](https://gitlab.com/yshukevich-examples/api)

### [Gitlab CI templates and pipelines](https://gitlab.com/yshukevich-examples/infrastructure)

### [Terraform Infrastructure](https://gitlab.com/yshukevich-examples/terraform-infrastructure)


## 🤝 Contacts

[![yaroslav.shukevich@gmail.com](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=whitelogoColor=white)](mailto:yaroslav.shukevich@gmail.com) [![linkedin](https://img.shields.io/badge/linkedin%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/yshukevich/) 
